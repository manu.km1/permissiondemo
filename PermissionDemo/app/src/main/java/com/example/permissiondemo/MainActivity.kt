package com.example.permissiondemo

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.permissiondemo.databinding.ActivityMainBinding
import java.io.IOException


class MainActivity : AppCompatActivity()
{

    private lateinit var binding : ActivityMainBinding
    private val REQUEST_PERMS = 1001
    private val PICK_PHOTO = 1002

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
            binding = ActivityMainBinding.inflate(LayoutInflater.from(this))
            setContentView(binding.root)

        if (hasRealRemovableSdCard(this)) {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                if(checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                {
                    requestPermissions(arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),REQUEST_PERMS)
                }
            }
        }
    }
    fun hasRealRemovableSdCard(context: Context): Boolean { // Code to check how many storage is there in mobile, if internal and external is there it will return true.
        return ContextCompat.getExternalFilesDirs(context, null).size >= 2
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)
    { // Callback function for checking permission is Granted or not.
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // This function is called by android its self
        if(requestCode == REQUEST_PERMS)
        {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                Log.d("","-------------------------PERMISSION_GRANTED----------------------------")
            }else
            {
                Log.d("","-------------------------PERMISSION_DENIED----------------------------")
            }
        }
    }
    fun onPickImage(view : View)
    {
        if (hasRealRemovableSdCard(this)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    AlertDialog.Builder(this)
                        .setMessage("To use this feature, Please grant file Access permission") // Right side Button in pop-up
                        .setPositiveButton("Go to Settings", DialogInterface.OnClickListener
                        { dialog, which ->
                            openAppSystemSettings()
                        })
                        .setNegativeButton("Cancel", null) // Left side Button in pop-up
                        .show()
                    return
                }
            }
        }
        val intent = Intent(Intent.ACTION_GET_CONTENT) //Starting the activity to pick the image
        intent.type = "image/*"
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_PHOTO)
   }

    //Code to pick the images
    // content://data/data/sdcard/photos/asda.jpg
    fun loadFromUri(photoUri: Uri?): Bitmap?
    {
        // Reading the images coming from Picker activity.     // content://data/data/sdcard/photos/asda.jpg
        var image: Bitmap? = null
        try
        {
            image = if (Build.VERSION.SDK_INT > 27)
            {
                val source = ImageDecoder.createSource(this.contentResolver, photoUri!!)
                ImageDecoder.decodeBitmap(source)
            } else
            {
                MediaStore.Images.Media.getBitmap(this.contentResolver, photoUri!!)
            }
        } catch(e: IOException)
        {
            e.printStackTrace()
        }

        return image
    }

    //Process the Image which come back from picker activity
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_PHOTO) {
            if (data?.data != null) { //Which means that user as successfully selected picked a image, if he cencels this will be null
                binding.imageView2.setImageBitmap(loadFromUri(data.data))
            }
        }
    }

}
fun Context.openAppSystemSettings() { /* This is the function will launch the Settings app for us and user directly go the details page he can click on permission
                                                and assign the permission */
    startActivity(Intent().apply{
        action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS // This will us to the details of the setting
        data = Uri.fromParts("package" , packageName,null) // This is our package manager
    })
}